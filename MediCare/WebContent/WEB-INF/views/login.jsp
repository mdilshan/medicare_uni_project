<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/public/static/css/login.css" />
    <link
      href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      rel="stylesheet"
      id="bootstrap-css"
    />
    <title>Login</title>
  </head>
  <body>
    <div class="sidenav">
      <div class="login-main-text">
        <h2>
          MEDI CARE<br />
          Login Page
        </h2>
        <p>Howdy stranger. log in to your account</p>
      </div>
    </div>
    <div class="main">
      <div class="col-md-6 col-sm-12">
        <div class="login-form">
          <form method="POST" action="${pageContext.request.contextPath}/login">
            <div class="form-group">
              <label>User Name</label>
              <input type="text" class="form-control" name="username" placeholder="User Name" value="${username}"/>
            </div>
            <div class="form-group">
              <label>Password</label>
              <input
                type="password"
                class="form-control"
                name="password"
                placeholder="Password"
                value= "${password}" 
              />
            </div>
    
            <%
            	if(request.getAttribute("errorString") != null) {
         
            %>
                 <div class="alert alert-danger" role="alert">
                	${errorString}
            	</div>		
            <% } %>
            <button type="submit" class="btn btn-black">Login</button>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>