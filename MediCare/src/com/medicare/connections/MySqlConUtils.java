package com.medicare.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlConUtils {

	public static Connection getSQLConnection() throws ClassNotFoundException, SQLException {
		String hostName = "127.0.0.1";
		String db = "medicareDev";
		String username = "root";
		String password = "root";
		return getSQLConnection(hostName, db, username, password);
	}

	public static Connection getSQLConnection(String hostname, String dbname, String username, String password)
			throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");

		String URI = "jdbc:mysql://" + hostname + ":3306/" + dbname;

		Connection conn = DriverManager.getConnection(URI, username, password);
		return conn;
	}

}
