package com.medicare.connections;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionUtils {

	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		// return MySqlConUtils.getSQLConnection();
		return PostgreSqlUtils.getSQLConnection();
	}

	public static void close(Connection conn) {
		try {
			conn.close();
		} catch (Exception e) {

		}
	}

	/**
	 * Undoes all changes made in the current transactionand releases any database
	 * locks currently heldby this Connection object. This method should beused only
	 * when auto-commit mode has been disabled.
	 * 
	 * call-site is JDBCFIlter.java
	 * 
	 * @param conn java.sql.Connection
	 */
	public static void rollback(Connection conn) {
		try {
			conn.rollback();
		} catch (Exception e) {

		}
	}

}
