package com.medicare.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class PostgreSqlUtils {

	public static Connection getSQLConnection() throws ClassNotFoundException, SQLException {
		String hostName = "127.0.0.1";
		String db = "medicareDev";
		String username = "postgres";
		String password = "root";
		return getSQLConnection(hostName, db, username, password);
	}

	public static Connection getSQLConnection(String hostname, String dbname, String username, String password)
			throws SQLException, ClassNotFoundException {
		Class.forName("org.postgresql.Driver");

		String URI = "jdbc:postgresql://" + hostname + "/" + dbname;
		Properties props = new Properties();
		props.setProperty("user", username);
		props.setProperty("password", password);
		props.setProperty("ssl", "false");

		Connection conn = DriverManager.getConnection(URI, props);
		return conn;
	}

}
