package com.medicare.beans;

public class Payment {
	private int id;
	private int cust_id;
	private int dct_id;
	private String service;
	private String cost;
	private String discount;
	private String type;
	
	public Payment() {}
	
	public Payment(
			int id,
			int cust_id,
			int dct_id,
			String service,
			String cost,
			String discount,
			String type
			) {
		this.id = id;
		this.cust_id = cust_id;
		this.dct_id = dct_id;
		this.service = service;
		this.cost = cost;
		this.discount = discount;
		this.type = type;
	}
	
	public Payment(Payment py) {
		this.cust_id = py.getCust_id();
		this.dct_id = py.getDct_id();
		this.service = py.getService();
		this.cost = py.getCost();
		this.discount = py.getDiscount();
		this.type = py.getType();
	}
	
	public int getCust_id() {
		return cust_id;
	}
	public void setCust_id(int cust_id) {
		this.cust_id = cust_id;
	}
	public int getDct_id() {
		return dct_id;
	}
	public void setDct_id(int dct_id) {
		this.dct_id = dct_id;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
