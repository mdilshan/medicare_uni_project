package com.medicare.beans;

import java.util.List;

public class UserAccount {
	public static final String GENDER_MALE = "M";
	public static final String GENDER_FEMALE = "F";

	private String username;
	private String password;
	private String gender;
	private Long user_id;

	private List<String> roles;

	public UserAccount() {
	}

	public UserAccount(String username, String password, String gender, String... roles) {
		if (roles != null) {
			for (String r : roles) {
				this.roles.add(r);
			}
		}
	}

	public void setId(Long id) {
		this.user_id = id;
	}

	public Long getId() {
		return this.user_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public List<String> getRoles() {
		return this.roles;
	}
}
