package com.medicare.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class authConfig {
	public enum authRoles {
		FrontDesk, Cashier, Doctor, Manager
	}

	private static final Map<authRoles, List<String>> routerMap = new HashMap<authRoles, List<String>>();

	static {
		init();
	}

	private static void init() {
		/**
		 * Authorized routes for FrontDesk
		 */
		List<String> urlPatternFrontDesk = new ArrayList<String>();
		urlPatternFrontDesk.add("/appointment");
		urlPatternFrontDesk.add("/addAppointment");

		/**
		 * Authorized routes for Cashier
		 */
		List<String> urlPatternCashier = new ArrayList<String>();
		urlPatternCashier.add("/payments");
		urlPatternCashier.add("/add-payment");

		/**
		 * Authorized routes for Doctor
		 */
		List<String> urlPatternDoctor = new ArrayList<String>();
		urlPatternDoctor.add("/payments");
		urlPatternDoctor.add("/add-payment");

		/**
		 * Authorized routes for Manager
		 */
		List<String> urlPatternManager = new ArrayList<String>();
		urlPatternManager.add("/rooms");
		urlPatternManager.add("/add-room");

		routerMap.put(authRoles.FrontDesk, urlPatternFrontDesk);
		routerMap.put(authRoles.Cashier, urlPatternCashier);
		routerMap.put(authRoles.Doctor, urlPatternDoctor);
		routerMap.put(authRoles.Manager, urlPatternManager);
	}

	public static Set<authRoles> AuthRoles() {
		return routerMap.keySet();
	}

	public static List<String> getAuthorizedUrls(authRoles role) {
		return routerMap.get(role);
	}
}
