package com.medicare.config;

public class constant {
	public static final String ISSUER = "medi-care-rest-api";
	public static final String SUBJECT = "medi-care-rest-api-jwt";
	public static final String AUDIENCE = "medicare.com";
}
