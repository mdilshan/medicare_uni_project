package com.medicare.restful;

import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.medicare.beans.UserAccount;
import com.medicare.restful.dto.LoginDto;
import com.medicare.services.AuthService;
import com.medicare.utils.DBUtils;

/**
 * This Controller use to handle Authentication and Authorization of the REST api
 * @author - Bandara IT19020372
 *
 */
@Path("/auth")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class AuthController {
	@Context
	ServletContext context;
	
	/**
	 * Generating valid JWT token to send to the front end for an user.
	 * @param login - username/email of the user
	 * @param password - password of the user
	 * @return
	 */
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(LoginDto login) {
		try {
			Connection conn = (Connection) context.getAttribute("pool");
			String hash = AuthService.digestPassword(login.password);
			UserAccount user = DBUtils.findUser(conn, login.username, hash);
			if (user == null) {
				HashMap<String, String> err = new HashMap<String, String>();
				err.put("message", "Invalid user");
				return Response.status(404).entity(err).build();
			}
			// TODO - get JWT toke
			String token = AuthService.Sign(null, user);
			JSONObject json = new JSONObject();
			json.put("accessToken", token);
			return Response.status(200).entity(json.toString()).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).build();
		}
	}

	/**
	 * This end point use to add new user to the system.
	 * @param login - email of the user
	 * @param password - password
	 * @param role - role of the user
	 * @return
	 */
	@POST
	@Path("/register")
	@Consumes(APPLICATION_FORM_URLENCODED)
	public Response register(
			@FormParam("username") String login, 
			@FormParam("password") String password,
			@FormParam("role") String role
		) {
		try {
			Connection conn = (Connection) context.getAttribute("pool");
			UserAccount user = new UserAccount();
			
			user.setUsername(login);
			List<String> roles = new ArrayList<String>();
			roles.add(role);
			user.setRoles(roles);
			
			String password_hash = AuthService.digestPassword(password);
			user.setPassword(password_hash);
			
			//Register the new user
			Long id = DBUtils.register(conn, user, role);

			//Get valid JWT token
			String token = AuthService.Sign(id, user);
			JSONObject json = new JSONObject();
			json.put("accetToken", token);
			json.put("user", user);
			return Response.status(200).entity(json.toString()).build();

		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).encoding(e.getMessage()).build();
		}
	}
}
