package com.medicare.restful;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.medicare.beans.Payment;
import com.medicare.services.PaymentService;

/**
 * This controller use to interact with the Payment functions.
 * 
 * @author Bandara - IT19020372
 *
 */
@Path("/payment")
public class PaymentController {
	@Context
	ServletContext context;
	
	/**
	 * Get list of payments. By using skip and limit,
	 * we can get different set of payments list 
	 * @param skip - query param 
	 * @param limit - query param
	 * @return
	 * 
	 */
	@GET
	@Path("/")
	@RequireAuth
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPayments(@QueryParam("skip") int skip, @QueryParam("limit") int limit) {
		Connection conn = (Connection)context.getAttribute("pool");
		String res;
		try {
			res = PaymentService.getPayments(conn, skip, limit);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JSONObject err = new JSONObject();
			err.put("message", e.getMessage());
			return Response.status(500).entity(err.toString()).build();
		}
		return Response.status(200).entity(res.toString()).build();
	}
	
	/**
	 * Get details about one payment when the payement id is provided
	 * @param id - url param
	 * @return
	 */
	@GET
	@Path("{id}")
	@RequireAuth
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOnePayment(@PathParam("id") int id) {
		Connection conn = (Connection)context.getAttribute("pool");
		String res;
		try {
			res = PaymentService.getOnePayment(conn, id);
		} catch(SQLException e) {
			JSONObject err = new JSONObject();
			err.put("message", e.getMessage());
			return Response.status(500).entity(err.toString()).build();			
		}
		return Response.status(200).entity(res.toString()).build();
	}
	
	/**
	 * Add a new payment to the system. The JSON object that send from the front end
	 * should be in type Payment.
	 * @param payment - casted request body.
	 * @return
	 */
	@POST
	@Path("/")
	@RequireAuth
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addPayment(Payment payment) {
		try {
			Connection conn = (Connection) context.getAttribute("pool");
			JSONObject res = PaymentService.addPayment(conn, payment);
			return Response.status(200).entity(res.toString()).build();
		} catch(Exception e) {
			JSONObject err = new JSONObject();
			err.put("message", e.getMessage());
			return Response.status(500).entity(err.toString()).build();
		}
	}
	
	/**
	 * Edit a payemnt. JSON body should able to cast to type payment
	 * @param id - id of the payment
	 * @param payment - updated payment object that casted from JSON payload
	 * @return
	 */
	@PUT
	@Path("{id}")
	@RequireAuth
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response editPayment(@PathParam("id") int id, Payment payment) {
		try {
			Connection conn = (Connection) context.getAttribute("pool");
			int row = PaymentService.editPayemtn(conn, payment, id);
			if(row < 1) {
				JSONObject err = new JSONObject();
				err.put("message", "Unable to update the payment");
				return Response.status(500).entity(err.toString()).build();
			}
			JSONObject res = new JSONObject();
			res.put("message", "Success");
			return Response.status(200).entity(res.toString()).build();
		} catch(Exception e) {
			JSONObject err = new JSONObject();
			err.put("message", e.getMessage());
			return Response.status(500).entity(err.toString()).build();
		}
	}
	
	/**
	 * Delete a payment from the database
	 * @param id -  id of the payment that need to delete
	 * @return
	 */
	@DELETE
	@Path("{id}")
	@RequireAuth
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletePayment(@PathParam("id") int id) {
		try {
			Connection conn = (Connection) context.getAttribute("pool");
			int row = PaymentService.deletePayment(conn, id);
			if(row < 1) {
				JSONObject err = new JSONObject();
				err.put("message", "Unable to update the payment");
				return Response.status(500).entity(err.toString()).build();
			}
			JSONObject res = new JSONObject();
			res.put("message", "Success");
			return Response.status(200).entity(res.toString()).build();
		} catch(Exception e) {
			JSONObject err = new JSONObject();
			err.put("message", e.getMessage());
			return Response.status(500).entity(err.toString()).build();
		}
	}
}
