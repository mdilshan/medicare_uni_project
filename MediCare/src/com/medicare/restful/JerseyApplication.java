package com.medicare.restful;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/api")
public class JerseyApplication extends ResourceConfig {
	public JerseyApplication() {
		/**
		 * Register CORS filter to run when Jersey start
		 */
		register(CORSFilter.class);
	}
}
