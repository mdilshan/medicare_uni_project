
package com.medicare.restful;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;

import com.medicare.services.AuthService;

/**
 * A basic auth gaurd for the application.
 * Whenever a request came in to the REST api it will check for JWT token and validate it.
 * This does not forward auth_user for the Controllers. It's just a basic auth gaurd
 * @author Bandara - IT19020372
 *
 */
@Provider
@RequireAuth
@Priority(Priorities.AUTHENTICATION)
public class RequireAuthFilter implements ContainerRequestFilter {

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		String token = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		if (token == null || !token.startsWith("Bearer ")) {
			throw new NotAuthorizedException("Authorization headers must be provided");
		}

		String jwt = token.substring("Bearer".length()).trim();

		try {
			boolean isValid = AuthService.Verify(jwt, "cashier");
			if (!isValid)
				throw new NotAuthorizedException("Invalid token");
		} catch (Exception e) {
			throw new NotAuthorizedException("Invalid token");
		}
	}
}
