package com.medicare.restful.dto;

/**
 * Data Transfer Object for Login process
 * @author Maduka
 *
 */
public class LoginDto {
	  public String username;
	  public String password;
}
