package com.medicare.restful;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.ws.rs.NameBinding;

/**
 * Use this interface to bind RequireAuthFilter to any Controller end point you want.
 * Do not register to the root. Just use whenever you want to use by using @requirAuth annotation
 * @author Bandara - IT19020372
 *
 */
@NameBinding
@Retention(RUNTIME)
@Target({ TYPE, METHOD })
public @interface RequireAuth {

}
