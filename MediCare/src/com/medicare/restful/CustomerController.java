package com.medicare.restful;


import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.medicare.services.CustomerService;

/**
 * @author Bandara - IT19020372
 *
 */
@Path("/customers")
public class CustomerController {
	@Context
	ServletContext context;
	/**
	 * Get list of customers for payment form.
	 * @return
	 * 
	 */
	@GET
	@Path("/")
	@RequireAuth
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDoctors() {
		Connection conn = (Connection) context.getAttribute("pool");
		String res;
		try {
			res = CustomerService.getCustomersJson(conn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JSONObject err = new JSONObject();
			err.put("message", e.getMessage());
			return Response.status(500).entity(err.toString()).build();
		}
		return Response.status(200).entity(res.toString()).build();
	}
}
