package com.medicare.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.medicare.beans.Payment;
import com.medicare.utils.ResultSetSerializer;

/**
 * This Service class contain all the business methods that need to implement Payment and Docotors
 * functionalities.
 * To reduce the total files and directories, SQL queuries are also added to here without creating
 * separate custom ORM system 
 * 
 * @author Bandara - IT19020372
 *
 */
public class PaymentService {
	public static String getPayments(Connection conn, int skip, int limit) 
			throws SQLException {
		try {
			
			// TODO - quering db and get data
			
			// Using direct SQL without using ORM to increase server perfomances
			
			String sql1 = "select" + 
					" p.id as payment_id," + 
					" d.id as doct_id," + 
					" c.id as cust_id," + 
					" p.service," + 
					" p.cost," + 
					" p.discount," + 
					" p.type," + 
					" concat(c.fname, ' ', c.lname) as cust_name," + 
					" c.mobile as cust_mobile," + 
					" c.gender as cust_gender," + 
					" c.email as cust_email," + 
					" concat(d.fname, ' ', d.lname) as doct_name," + 
					" d.special as doct_special," + 
					" d.mobile as doct_mobile," + 
					" d.gender as doct_gender," + 
					" count(*) OVER() AS full_count " +
					" from payment p" + 
					" inner join doctor d" + 
					"  on p.dct_id = d.id" + 
					" inner join customers c" + 
					"  on c.id = p.cust_id" + 
					" limit ?" + 
					" offset ?";
			
			PreparedStatement pstm = conn.prepareStatement(sql1);
			pstm.setInt(1, limit);
			pstm.setInt(2, skip);
			
			
			SimpleModule module = new SimpleModule();
			module.addSerializer(new ResultSetSerializer());
			
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.registerModule(module);
			
			ResultSet rs = pstm.executeQuery();
			
			ObjectNode objectNode = objectMapper.createObjectNode();
			
			objectNode.putPOJO("results", rs);
			String data = "";
			try {
				data = objectMapper.writeValueAsString(objectNode);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return data;
		} catch(SQLException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	public static String getOnePayment(Connection conn, int payment_id) throws SQLException {
		try {
			String sql1 = "select" + 
					" p.id as payment_id," + 
					" d.id as doct_id," + 
					" c.id as cust_id," + 
					" p.service," + 
					" p.cost," + 
					" p.discount," + 
					" p.type," + 
					" concat(c.fname, ' ', c.lname) as cust_name," + 
					" c.mobile as cust_mobile," + 
					" c.dob as cust_dob," +
					" c.gender as cust_gender," + 
					" c.email as cust_email," + 
					" c.address as cust_address," + 
					" c.img as cust_avatar,"+
					" concat(d.fname, ' ', d.lname) as doct_name," + 
					" d.special as doct_special," +
					" d.experience as doct_experience," +
					" d.mobile as doct_mobile," + 
					" d.gender as doct_gender," + 
					" d.dob as doct_dob," +
					" d.email as doct_email," + 
					" d.description as doct_desc," + 
					" d.img as doct_avatar" +
					" from payment p" + 
					" inner join doctor d" + 
					"  on p.dct_id = d.id" + 
					" inner join customers c" + 
					"  on c.id = p.cust_id" + 
					" where p.id = ?";
			
			PreparedStatement pstm = conn.prepareStatement(sql1);
			pstm.setInt(1, payment_id);
			
			SimpleModule module = new SimpleModule();
			module.addSerializer(new ResultSetSerializer());
			
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.registerModule(module);
			
			ResultSet rs = pstm.executeQuery();
			
			ObjectNode objectNode = objectMapper.createObjectNode();
			
			objectNode.putPOJO("results", rs);
			String data = "";
			try {
				data = objectMapper.writeValueAsString(objectNode);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return data;
			
		} catch(SQLException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	/**
	 * Add new payment to the system.
	 * @param conn - DB connection
	 * @param payment 
	 * @return
	 * @throws SQLException
	 */
	public static JSONObject addPayment(Connection conn, Payment payment) throws SQLException {
		try {
			String sql = "insert into Payment " + 
					"(cust_id, dct_id, service, cost, discount, type) " + 
					"values " + 
					"(?,?,?,?,?,?)";
			
			PreparedStatement pstm = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
		
			
			pstm.setInt(1, payment.getCust_id());
			pstm.setInt(2, payment.getDct_id());
			pstm.setString(3, payment.getService());
			pstm.setString(4, payment.getCost());
			pstm.setString(5, payment.getDiscount());
			pstm.setString(6, payment.getType());
			
			JSONObject data = new JSONObject();
			
			int affectedRows = pstm.executeUpdate();
			if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstm.getGeneratedKeys()) {
                    if (rs.next()) {
                    	Payment cpy = new Payment(payment);
                    	cpy.setId(rs.getInt(1));
            			data.put("payment", cpy);
            			
                    }
                } catch (SQLException ex) {
                	data.put("message", ex.getMessage());
                }
            }
			return data;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
	}
	
	public static int editPayemtn(Connection conn, Payment payment, int id) throws SQLException {
		String sql = " update payment\r\n" + 
				" set cust_id = ? , dct_id = ? , service = ? , cost = ? , discount = ? , type = ?\r\n" + 
				" where id = ?";
		int affectedrows = 0;
		
		try (PreparedStatement pstm = conn.prepareStatement(sql)){
			pstm.setInt(1, payment.getCust_id());
			pstm.setInt(2, payment.getDct_id());
			pstm.setString(3, payment.getService());
			pstm.setString(4, payment.getCost());
			pstm.setString(5, payment.getDiscount());
			pstm.setString(6, payment.getType());
			pstm.setInt(7, id);
			
			affectedrows = pstm.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return affectedrows;
	}
	
	public static int deletePayment(Connection conn, int id) throws SQLException {
		String sql = "delete from payment where id = ?;";
		int affectedrows = 0;
		try (PreparedStatement pstm = conn.prepareStatement(sql)){
			pstm.setInt(1, id);	
	
			affectedrows = pstm.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return affectedrows;
	}
}
