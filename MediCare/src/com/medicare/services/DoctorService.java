package com.medicare.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.medicare.utils.ResultSetSerializer;

/**
 * 
 * @author Bandara - IT19020372
 *
 */
public class DoctorService {
	public static String getDoctorsJson(Connection conn) throws SQLException {
		String sql = "Select * from doctor";
		try {
			PreparedStatement pstm = conn.prepareStatement(sql);
			
			SimpleModule module = new SimpleModule();
			module.addSerializer(new ResultSetSerializer());
			
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.registerModule(module);
			
			ResultSet rs = pstm.executeQuery();
			
			ObjectNode objectNode = objectMapper.createObjectNode();
			
			objectNode.putPOJO("results", rs);
			String data = "";
			try {
				data = objectMapper.writeValueAsString(objectNode);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return data;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
	}
}
