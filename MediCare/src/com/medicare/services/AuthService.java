package com.medicare.services;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;


import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.medicare.beans.UserAccount;
import com.medicare.config.constant;

/**
 * The AuthService class contains all the methods that related to authentication 
 * and authorization process of the RESTapi.
 * Every methods in the service class are public and static.
 * By making them static and making them public I can use them kind of similar way when
 * inject Service files to the root level of the application when use Dependacy Injection
 * 
 * @author Bandara - IT19020372
 *
 */
public class AuthService {

	/**
	 * Generating a Json web token. This is very basic method to generate JWT.
	 * According to the project scope this method is enough to full fill the
	 * requirements
	 * 
	 * @return String - Genarated JSON web token.
	 * 
	 * @author Bandara - IT19020372
	 */
	public static String Sign(Long id, UserAccount acc) {
		try {
			Long user_id = id != null ? id  : 0;
			String username = acc.getUsername();
			
			 // Ignoring other roles. By Assume there's only one role per user
			String role = acc.getRoles().get(0);
			
			// Using one key algorithm for simplicity.
			Algorithm algorithm = Algorithm.HMAC256("secret");
			
			 String token = JWT.create()
					 .withIssuer(constant.ISSUER)
					 .withSubject(constant.SUBJECT)
					 .withAudience(constant.AUDIENCE)
					 .withClaim("user_id", user_id)
					 .withClaim("username", username)
					 .withClaim("role", role)
					 .sign(algorithm);

			return token;
		} catch (JWTCreationException exception) {
			exception.printStackTrace();
			return exception.getMessage();
		}
	}

	/**
	 * This method use to verify a Json web token. If valid returns true; else
	 * return false.
	 * 
	 * @param token - JWT token
	 * @param role  - the user role which need to validate
	 * @return Boolean
	 * @author Bandara - IT19020372
	 */
	public static boolean Verify(String token, String role) {
		try {
			
			Algorithm algorithm = Algorithm.HMAC256("secret");
			JWTVerifier verifier = JWT.require(algorithm)
					.withIssuer(constant.ISSUER)
					.withSubject(constant.SUBJECT)
					.withAudience(constant.AUDIENCE)
					.build(); // Reusable verifier instance
			DecodedJWT jwt = verifier.verify(token);
			String username = jwt.getClaim("username").asString();
			String role_in_token = jwt.getClaim("role").asString();

			if (username != null && role_in_token.equals(role))
				return true;

			return false;
		} catch (JWTVerificationException exception) {
			return false;
		}
	}

	/**
	 * Return a hashed String of a given string by using SHA-256.
	 * This does not include salting so the hashed value for the same String will
	 * be also the same everytime when try to hash it. 
	 * @param plainTextPassword - the string to hash
	 * @return hashed password - string
	 */
	public static String digestPassword(String plainTextPassword) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(plainTextPassword.getBytes("UTF-8"));
			byte[] passwordDigest = md.digest();
			return new String(Base64.getEncoder().encode(passwordDigest));
		} catch (Exception e) {
			throw new RuntimeException("Exception encoding password", e);
		}
	}

	public static boolean useList(String[] arr, String targetValue) {
		return Arrays.asList(arr).contains(targetValue);
	}
}
