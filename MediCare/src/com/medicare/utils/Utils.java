package com.medicare.utils;

import java.sql.Connection;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.medicare.beans.UserAccount;
import com.medicare.config.authConfig;

public class Utils {

	public static final String __CONN_ATT__ = "DB_CON";
	private static final String __USER_ATT__ = "USER_NAME_IN_COOKIE";

	/**
	 * Store database connection in the servlet request @param(req) - The request
	 * object @param(conn) - Database connection
	 */
	public static void storeDBConn(ServletRequest req, Connection conn) {
		req.setAttribute(__CONN_ATT__, conn);
	}

	/**
	 * Return the connection stored in servlet connection
	 */
	public static Connection getConnFromReq(ServletRequest req) {
		Connection conn = (Connection) req.getAttribute(__CONN_ATT__);
		return conn;
	}

	/**
	 * Store user info in Session.
	 */
	public static void storeLoggedInUser(HttpSession session, UserAccount loginedUser) {
		// IN JSP can access -> ${loggedInUser}
		session.setAttribute("loggedInUser", loginedUser);
	}

	/**
	 * Get the user information stored in the session.
	 */
	public static UserAccount getLoginedUser(HttpSession session) {
		UserAccount loggedInUser = (UserAccount) session.getAttribute("loggedInUser");
		return loggedInUser;
	}

	/**
	 * Store info in Cookie
	 */
	public static void storeUserCookie(HttpServletResponse response, UserAccount user) {
		System.out.println("Store user cookie");
		Cookie cookieUserName = new Cookie(__USER_ATT__, user.getUsername());
		// 1 day (Converted to seconds)
		cookieUserName.setMaxAge(24 * 60 * 60);
		response.addCookie(cookieUserName);
	}

	public static String getUserNameInCookie(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (__USER_ATT__.equals(cookie.getName())) {
					return cookie.getValue();
				}
			}
		}
		return null;
	}

	public static void deleteCookie(HttpServletResponse response) {
		Cookie cookieUserName = new Cookie(__USER_ATT__, null);
		// setting 0 seconds to expire it immediately
		cookieUserName.setMaxAge(0);
		response.addCookie(cookieUserName);
	}

	/**
	 * Check if a requested url is a secured one or not
	 */
	public static boolean isRequireAuth(HttpServletRequest request) {
		String urlPattern = getUrlPattern(request);
		Set<authConfig.authRoles> roles = authConfig.AuthRoles();

		for (authConfig.authRoles role : roles) {
			List<String> urlPatterns = authConfig.getAuthorizedUrls(role);
			if (urlPatterns != null && urlPatterns.contains(urlPattern)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isAuthorized(HttpServletRequest request) {
		String urlPattern = getUrlPattern(request);

		Set<authConfig.authRoles> allRoles = authConfig.AuthRoles();

		for (authConfig.authRoles role : allRoles) {
			if (!request.isUserInRole(role.name())) {
				continue;
			}
			List<String> urlPatterns = authConfig.getAuthorizedUrls(role);
			if (urlPatterns != null && urlPatterns.contains(urlPattern)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get url pattern of a http request
	 */
	public static String getUrlPattern(HttpServletRequest request) {
		ServletContext servletContext = request.getServletContext();
		String servletPath = request.getServletPath();
		String pathInfo = request.getPathInfo();

		String urlPattern = servletPath;
		if (pathInfo != null) {
			urlPattern += "/*";
			return urlPattern;
		}
		// if path infor is empty - check if it still direct to a servlet
		if (isServlet(servletContext, urlPattern))
			return urlPattern;

		// if it is not a servlet it should be a file so check last .(dot) to check if
		// it is a file or not
		int i = servletPath.lastIndexOf('.');
		if (i != -1) {
			urlPattern = "*." + servletPath.substring(i + 1); // *.xyz
			if (isServlet(servletContext, urlPattern))
				return urlPattern;
		}
		return "/";
	}

	/**
	 * Check if a particular url pattern is directed to a Servlet or not
	 */
	public static Boolean isServlet(ServletContext servletContext, String urlPattern) {
		Map<String, ? extends ServletRegistration> map = servletContext.getServletRegistrations();
		for (String servletName : map.keySet()) {
			ServletRegistration sr = map.get(servletName);

			Collection<String> mappings = sr.getMappings();
			if (mappings.contains(urlPattern)) {
				return true;
			}

		}
		return false;
	}

	private static int REDIRECT_ID = 0;

	private static final Map<Integer, String> id_uri_map = new HashMap<Integer, String>();
	private static final Map<String, Integer> uri_id_map = new HashMap<String, Integer>();

	public static int storeRedirectAfterLoginUrl(HttpSession session, String requestUri) {
		Integer id = uri_id_map.get(requestUri);

		if (id == null) {
			id = REDIRECT_ID++;

			uri_id_map.put(requestUri, id);
			id_uri_map.put(id, requestUri);
			return id;
		}

		return id;
	}

	public static String getRedirectAfterLoginUrl(HttpSession session, int redirectId) {
		String url = id_uri_map.get(redirectId);
		if (url != null) {
			return url;
		}
		return null;
	}
}
