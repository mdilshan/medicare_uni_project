package com.medicare.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.medicare.beans.UserAccount;
/**
 * These utils are use for authentication process of the applicaiton
 * @author Bandara - IT19020372
 *
 */
public class DBUtils {
	public static UserAccount findUser(Connection conn, String username, String password) throws SQLException {
		String sql = "Select a.id, a.user_name, a.password, a.role " + "from user_account a "
				+ "where a.user_name = ? and a.password = ?";
		try {
			PreparedStatement pstm = conn.prepareStatement(sql);
			pstm.setString(1, username);
			pstm.setString(2, password);
			ResultSet rs = pstm.executeQuery();
			
			if (rs.next()) {
				List<String> role = new ArrayList<String>();
				role.add(rs.getString(4));
				UserAccount user = new UserAccount();
				user.setUsername(username);
				user.setPassword(password);
				user.setRoles(role);
				//user.setId(rs.getLong("id"));
				return user;
			}
			return null;
		} catch(SQLException ex) {
			
			ex.printStackTrace();
			return null;
		}
	}

	public static UserAccount findUser(Connection conn, String username) throws SQLException {
		String sql = "select a.user_name, a.password " + "from user_account a " + "where a.user_name = ?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, username);
		ResultSet rs = pstm.executeQuery();

		if (rs.next()) {
			String password = rs.getString("password");
			String gender = rs.getString("Gender");
			UserAccount user = new UserAccount();
			user.setUsername(username);
			user.setPassword(password);
			user.setGender(gender);
			return user;
		}

		return null;
	}

	public static long register(Connection conn, UserAccount user, String role) {
		String sql = "insert into user_account (user_name, password, role) values (?, ?, ?)";
		long id = 0;
		try {
			PreparedStatement pstm = conn.prepareStatement(sql);
			pstm.setString(1, user.getUsername());
			pstm.setString(2, user.getPassword());
			pstm.setString(3, role);

			int affectedRows = pstm.executeUpdate();
			// pstm.executeQuery();
			// check the affected rows
			if (affectedRows > 0) {
				// get the ID back
				try (ResultSet rs = pstm.getGeneratedKeys()) {
					if (rs.next()) {
						id = rs.getLong(1);
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return id;
	}
}
