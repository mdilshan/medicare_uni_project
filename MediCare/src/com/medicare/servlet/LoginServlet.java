package com.medicare.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.medicare.beans.UserAccount;
import com.medicare.utils.DBUtils;
import com.medicare.utils.Utils;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher //
				= this.getServletContext().getRequestDispatcher("/WEB-INF/views/login.jsp");

		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		Connection conn = Utils.getConnFromReq(request);
		UserAccount user = null;
		String errorString = null;
		Boolean isError = false;
		try {
			user = DBUtils.findUser(conn, username, password);
			if (user == null) {
				isError = true;
				errorString = "Username or Password not valid";
			}
		} catch (SQLException e) {
			e.printStackTrace();
			isError = true;
			errorString = e.getMessage();
		}

		if (isError) {
			request.setAttribute("username", username);
			request.setAttribute("password", password);
			request.setAttribute("errorString", errorString);
			doGet(request, response);
		} else {
			HttpSession session = request.getSession();
			Utils.storeLoggedInUser(session, user);
			Utils.storeUserCookie(response, user);

			response.sendRedirect(request.getContextPath() + "/home");
		}

	}

}
