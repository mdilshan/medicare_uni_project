package com.medicare.servlet;

import java.io.IOException;

import javax.annotation.security.RolesAllowed;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet(name = "HomeServlet", urlPatterns = { "/home" })
@RolesAllowed({ "admin" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// This method is always called once after the Servlet object is created.
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		System.out.println("Servlet works");
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("doGet works");
		RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/homeView.jsp");
		dispatcher.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) {
		//
	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) {
		// edit
	}
}
