package com.medicare.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SpaServlet.
 * 
 * The only purpose of this servlet is to serve spa.jsp file which is contain an Angular application.
 * Routing of Payment application is handle by, client side routing using Angular Router Module.
 * By using Angular HTTP Module, payment application is communicate with Jersey Application to
 * GET/POST/PUT/DELETE data. 
 * 
 * Please check REST api ( JERSEY APP ) to find CRUD operations
 * 
 * @author Bandara - IT19020372
 */
@WebServlet(name = "SpaServlet", urlPatterns = { "/spa/*" })
public class SpaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/spa.jsp");
		dispatcher.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
