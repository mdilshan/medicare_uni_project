package com.medicare.filters;

import java.io.IOException;
import java.sql.Connection;
import java.util.Collection;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import com.medicare.connections.ConnectionUtils;
import com.medicare.utils.Utils;

@WebFilter(filterName = "jdbcFilter", urlPatterns = { "/fake-end-point-disbale-filter" })
public class JDBCFilter implements Filter {
	public JDBCFilter() {
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {

	}

	@Override
	public void destroy() {
	}

	/**
	 * Reqeust for image/css/js are not required to open a db connection This filter
	 * is use to determine if the request is directed to a servlet or not. Call-site
	 * is this.doFilter();
	 * 
	 * call-site is this.doFilter()
	 */
	private boolean needJDBC(HttpServletRequest req) {
		/**
		 * if url -> http://localhost:8080/medicare/account/login?username=password
		 * /account/* -> Servlet url-pattern
		 */

		// --> /account
		String servletPath = req.getServletPath();
		// --> /login
		String pathInfo = req.getPathInfo();

		String urlPattern = servletPath;

		if (pathInfo != null) {
			// --> /account/*
			urlPattern = servletPath + "/*";
		}

		// The returned Map includes the ServletRegistration objectscorresponding to all
		// declared and annotated servlets
		Map<String, ? extends ServletRegistration> servletRegistrations = req.getServletContext()
				.getServletRegistrations();

		Collection<? extends ServletRegistration> values = servletRegistrations.values();

		// loop through values and check if the url pattern is in there.
		for (ServletRegistration sr : values) {
			Collection<String> map = sr.getMappings();
			if (map.contains(urlPattern)) {
				// urlPattern is directed to a servlet so return true;
				return true;
			}
		}
		return false;
	}

	/**
	 * Here we open a connection for only servlet request and other reqest are chain
	 * wihout open connections
	 */
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;

		/**
		 * Only open connection for the Servlet or JSP Avoid connection for common files
		 * like images, css, js
		 */
		if (this.needJDBC(request)) {
			System.out.println("Open connection for: " + request.getServletPath());

			Connection conn = null;
			try {
				conn = ConnectionUtils.getConnection();
				// If a connection is in auto-commit mode, then all its SQLstatements will be
				// executed and committed as individualtransactions
				conn.setAutoCommit(false);
				Utils.storeDBConn(req, conn);
				chain.doFilter(req, res);
				conn.commit();
			} catch (Exception e) {
				e.printStackTrace();
				ConnectionUtils.rollback(conn);
				throw new ServletException();
			} finally {
				ConnectionUtils.close(conn);
			}
		} else {
			chain.doFilter(req, res);
		}
	}
}
