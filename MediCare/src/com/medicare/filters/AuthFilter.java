package com.medicare.filters;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.medicare.beans.UserAccount;
import com.medicare.utils.DBUtils;
import com.medicare.utils.Utils;

/**
 * Cookie key is set to expire within 1 day. If the user tick remember-me then
 * use this filter to re-new the cookies
 */
@WebFilter(filterName = "authFilter", urlPatterns = { "/*" })
public class AuthFilter implements Filter {
	public AuthFilter() {
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {

	}

	@Override
	public void destroy() {
	};

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		String servletPath = req.getServletPath();
		if (servletPath.startsWith("/api/")) {
			chain.doFilter(req, res);
			return;
		}
		HttpSession session = req.getSession();

		UserAccount sessionUser = Utils.getLoginedUser(session);
		if (sessionUser != null) {
			session.setAttribute("COOKIE_CHECKED_STATUS", "CHECKED");
			chain.doFilter(request, response);
			return;
		}

		// connection was created in JDBCFilter;
		Connection conn = Utils.getConnFromReq(req);

		String checked = (String) session.getAttribute("COOKIE_CHECKED_STATUS");

		if (checked == null && conn != null) {
			String username = Utils.getUserNameInCookie(req);
			try {
				UserAccount user = DBUtils.findUser(conn, username);
				Utils.storeLoggedInUser(session, user);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			session.setAttribute("COOKIE_CHECKED_STATUS", "CHECKED");
		}
		chain.doFilter(request, response);
	}
}
