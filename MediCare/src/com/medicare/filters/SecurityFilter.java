package com.medicare.filters;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.medicare.beans.UserAccount;
import com.medicare.utils.UserRoleWrapper;
import com.medicare.utils.Utils;

/**
 * Servlet Filter implementation class SecurityFilter
 */
@WebFilter(filterName = "securityFilter", urlPatterns = { "/*" })
public class SecurityFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public SecurityFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		String servletPath = req.getServletPath();

		if (servletPath.equals("/login")) {
			chain.doFilter(req, res);
			return;
		}

		if (servletPath.startsWith("/api/")) {
			chain.doFilter(req, res);
			return;
		}

		HttpSession session = req.getSession();
		UserAccount sessionUser = Utils.getLoginedUser(session);

		HttpServletRequest reqCopy = req;
		if (sessionUser != null) {
			String userName = sessionUser.getUsername();
			List<String> roles = sessionUser.getRoles();
			reqCopy = new UserRoleWrapper(userName, roles, req);
		}

		if (Utils.isRequireAuth(req)) {
			if (sessionUser == null) {

				String requestUri = req.getRequestURI();

				// Store the current page to redirect to after successful login.
				int redirectId = Utils.storeRedirectAfterLoginUrl(session, requestUri);

				res.sendRedirect(reqCopy.getContextPath() + "/login?redirectId=" + redirectId);
				return;
			}

			boolean isAuthorized = Utils.isAuthorized(reqCopy);
			if (!isAuthorized) {

				RequestDispatcher dispatcher //
						= request.getServletContext().getRequestDispatcher("/WEB-INF/views/accessDeniedView.jsp");

				dispatcher.forward(request, response);
				return;
			}
		}
		chain.doFilter(reqCopy, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
