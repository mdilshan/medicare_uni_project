package com.medicare.listners;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.medicare.connections.PostgreSqlUtils;

/**
 * Create database connection pool for better perfomance. 
 *
 * @author Bandara - IT19020372
 */
@WebListener
public class DBConnPool implements ServletContextListener {

	public static Connection conn;

	/**
	 * Default constructor.
	 */
	public DBConnPool() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		try {
			conn = PostgreSqlUtils.getSQLConnection();
			// Add connection to servlet context
			arg0.getServletContext().setAttribute("pool", conn);
			System.out.println("Open pool con");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
